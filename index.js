require('dotenv/config');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const routes = require('./src/routes/index.js');
const { connect } = require('./config/database');

let app = express();

connect();

const port = process.env.PORT || 8080;

app.use(cors());
app.use(bodyParser.json());

app = routes.loadRoutes(app);

app.listen(port, () => {
    console.log('server on')
});

module.exports = app;