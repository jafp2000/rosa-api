require('dotenv/config');
const mongooseLib = require('mongoose');
const UserSeeder = require('./seeders/users.seeder.js');
const BoardSeeder = require('./seeders/board.seeder.js');
const WordSeeder = require('./seeders/word.seeder.js');

const { connectionString } = require('./config/database')

mongooseLib.Promise = global.Promise || Promise;

module.exports = {
  mongoose: mongooseLib,

  mongoURL: connectionString,

  seedersList: {
    users: UserSeeder,
    boards: BoardSeeder,
    words: WordSeeder
  },
};