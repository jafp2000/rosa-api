const mongoose = require('mongoose');

const connectionString = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`


const connect = () => {
    mongoose.connect(connectionString, { useNewUrlParser: true })
    .then(() => {
        console.log('mongo connected')
    })
    .catch((err) => {
        console.log('ERROR', err)
    });

}

module.exports = { connect, connectionString };
