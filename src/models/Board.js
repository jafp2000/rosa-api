const mongoose = require('mongoose');

const boardSchema = mongoose.Schema({
    description: String,
    status: {
        type: Boolean,
        default: true
    }
});

const board = module.exports = mongoose.model('board', boardSchema);