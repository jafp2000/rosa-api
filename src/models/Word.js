const mongoose = require('mongoose');

const wordSchema = mongoose.Schema({
    description: String,
    status: {
        type: Boolean,
        default: true
    }
});

const word = module.exports = mongoose.model('word', wordSchema);