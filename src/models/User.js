const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    name: String,
    email: String,
    score: Number,
    status: {
        type: Boolean,
        default: true
    }
});

const User = module.exports = mongoose.model('user', userSchema);