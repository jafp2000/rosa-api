const express = require('express');
const UserController = require('../controllers/UserController');
const UserValidator = require('../validators/UserValidator');

const router = express.Router();

router.get('/', UserController.index);
router.get('/:id', UserController.show);
router.post('/', UserValidator.store, UserController.store);
router.put('/:id', UserValidator.update, UserController.update);
router.delete('/:id', UserController.delete);

module.exports = router;