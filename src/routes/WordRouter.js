const express = require('express');
const WordController = require('../controllers/WordController');
const WordValidator = require('../validators/WordValidator');

const router = express.Router();

router.get('/', WordController.index);
router.get('/:id', WordController.show);
router.post('/', WordValidator.store, WordController.store);
router.put('/:id', WordValidator.update, WordController.update);
router.delete('/:id', WordController.delete);

module.exports = router;