const express = require('express');
const BoardController = require('../controllers/BoardController');
const BoardValidator = require('../validators/BoardValidator');

const router = express.Router();

router.get('/', BoardController.index);
router.get('/:id', BoardController.show);
router.post('/', BoardValidator.store, BoardController.store);
router.put('/:id', BoardValidator.update, BoardController.update);
router.delete('/:id', BoardController.delete);

module.exports = router;