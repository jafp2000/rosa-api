const userRouter = require('./UserRouter');
const boardRouter = require('./BoardRouter');
const wordRouter = require('./WordRouter');

exports.loadRoutes = (app) => {
    app.use('/users', userRouter)
    app.use('/boards', boardRouter)
    app.use('/words', wordRouter)

    return app
};