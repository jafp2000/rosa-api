const { check } = require('express-validator/check');

exports.store = [
    check('description').exists()
]

exports.update = [
    check('description').exists()
]