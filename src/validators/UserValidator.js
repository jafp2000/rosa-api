const { check } = require('express-validator/check');

exports.store = [
    check('name').exists(),
    check('email').isEmail(),
    check('score').isNumeric()
]

exports.update = [
    check('name').exists(),
    check('email').isEmail(),
    check('score').isNumeric()
]