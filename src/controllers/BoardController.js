'use strict'

const { validationResult } = require('express-validator/check');
const Board = require('../models/Board');

class BoardController {
    constructor(){
        this.index = this.index.bind(this);
        this.show = this.show.bind(this);
        this.store = this.store.bind(this);
        this.update = this.update.bind(this);
        this.delete = this.delete.bind(this);
    }

    async index(req, res, next){
        let data = []

        data = await Board.find({status: true});

        return res.status(200).json({
            status: 200,
            message: "Board's found",
            data
        });
    }

    async show(req, res, next){
        const id = req.params.id;

        const data = await Board.findById(id);

        return res.status(200).json({
            status: 200,
            message: "Board found",
            data
        });
    }

    async store(req, res, next){
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(422).json({
                code: 422,
                message: "Validation error",
                errors: errors.array()
            });
        }

        const body = {
            description: req.body.description
        };

        let data = new Board(body);

        data = await data.save(body);

        return res.status(200).json({
            status: 200,
            message: "Board created",
            data
        });
    }

    async update(req, res, next){
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(422).json({
                code: 422,
                message: "Validation error",
                errors: errors.array()
            });
        }

        const id = req.params.id;

        const body = {
            description: req.body.description
        };

        let data = await Board.findById(id);

        data = await data.save(body);

        return res.status(200).json({
            status: 200,
            message: "Board updated",
            data
        });
    }

    async delete(req, res, next){
        const id = req.params.id;

        let data = await Board.findById(id);

        data.status = false;

        data = await data.save(body);

        return res.status(200).json({
            status: 200,
            message: "Board deleted",
            data
        });
    }
}

module.exports = new BoardController();
