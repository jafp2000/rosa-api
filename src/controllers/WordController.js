'use strict'

const { validationResult } = require('express-validator/check');
const Word = require('../models/Word');

class WordController {
    constructor(){
        this.index = this.index.bind(this);
        this.show = this.show.bind(this);
        this.store = this.store.bind(this);
        this.update = this.update.bind(this);
        this.delete = this.delete.bind(this);
    }

    async index(req, res, next){
        let data = []

        data = await Word.find({status: true});

        return res.status(200).json({
            status: 200,
            message: "Word's found",
            data
        });
    }

    async show(req, res, next){
        const id = req.params.id;

        const data = await Word.findById(id);

        return res.status(200).json({
            status: 200,
            message: "Word found",
            data
        });
    }

    async store(req, res, next){
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(422).json({
                code: 422,
                message: "Validation error",
                errors: errors.array()
            });
        }

        const body = {
            description: req.body.description
        };

        let data = new Word(body);

        data = await data.save(body);

        return res.status(200).json({
            status: 200,
            message: "Word created",
            data
        });
    }

    async update(req, res, next){
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(422).json({
                code: 422,
                message: "Validation error",
                errors: errors.array()
            });
        }

        const id = req.params.id;

        const body = {
            description: req.body.description
        };

        let data = await Word.findById(id);

        data = await data.save(body);

        return res.status(200).json({
            status: 200,
            message: "Word updated",
            data
        });
    }

    async delete(req, res, next){
        const id = req.params.id;

        let data = await Word.findById(id);

        data.status = false;

        data = await data.save(body);

        return res.status(200).json({
            status: 200,
            message: "Word deleted",
            data
        });
    }
}

module.exports = new WordController();
