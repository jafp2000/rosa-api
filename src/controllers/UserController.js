'use strict'

const { validationResult } = require('express-validator/check');
const User = require('../models/User');

class UserController {
    constructor(){
        this.index = this.index.bind(this);
        this.show = this.show.bind(this);
        this.store = this.store.bind(this);
        this.update = this.update.bind(this);
        this.delete = this.delete.bind(this);
    }

    async index(req, res, next){
        let data = []

        data = await User.find({status: true});

        return res.status(200).json({
            status: 200,
            message: "User's found",
            data
        });
    }

    async show(req, res, next){
        const id = req.params.id;

        const data = await User.findById(id);

        return res.status(200).json({
            status: 200,
            message: "User found",
            data
        });
    }

    async store(req, res, next){
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(422).json({
                code: 422,
                message: "Validation error",
                errors: errors.array()
            });
        }

        const body = {
            name: req.body.name,
            email: req.body.email,
            score: req.body.score
        };

        let data = new User(body);

        data = await data.save(body);

        return res.status(200).json({
            status: 200,
            message: "User created",
            data
        });
    }

    async update(req, res, next){
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(422).json({
                code: 422,
                message: "Validation error",
                errors: errors.array()
            });
        }

        const id = req.params.id;

        const body = {
            name: req.body.name,
            email: req.body.email,
            score: req.body.score
        };

        let data = await User.findById(id);

        data = await data.save(body);

        return res.status(200).json({
            status: 200,
            message: "User updated",
            data
        });
    }

    async delete(req, res, next){
        const id = req.params.id;

        let data = await User.findById(id);

        data.status = false;

        data = await data.save(body);

        return res.status(200).json({
            status: 200,
            message: "User deleted",
            data
        });
    }
}

module.exports = new UserController();
