const Seeder = require('mongoose-data-seed').Seeder;
const Model = require('../src/models/User');

var UsersSeeder = Seeder.extend({
  shouldRun: function () {
    return Model.countDocuments().exec().then(count => count === 0);
  },

  run: function () {
    const data = [{
      name: "Rosa Piña",
      email: "rosa@gmail.com",
      score: 0
    }]

    return Model.create(data);
  }
});

module.exports = UsersSeeder;
