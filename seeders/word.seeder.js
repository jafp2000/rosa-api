const Seeder = require('mongoose-data-seed').Seeder;
const Model = require('../src/models/Word');

var WordSeeder = Seeder.extend({
  shouldRun: function () {
    return Model.countDocuments().exec().then(count => count === 0);
  },

  run: function () {
    const data = [{
      description: "example"
    }]

    return Model.create(data);
  }
});

module.exports = WordSeeder;
