const Seeder = require('mongoose-data-seed').Seeder;
const Model = require('../src/models/Board');

var BoardSeeder = Seeder.extend({
  shouldRun: function () {
    return Model.countDocuments().exec().then(count => count === 0);
  },

  run: function () {
    const data = [{
      description: "A"
    }]

    return Model.create(data);
  }
});

module.exports = BoardSeeder;
